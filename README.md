2019 ARMOUR PEAK LLC - Brian Hanson

Use the Jamf API to leverage the patch definitions for latest version of macOS
Once we have the version, call softwareupdate to install this OS patch only
We may or may not have the latest patch from Apple already cached, but that's ok
Make sure to create a limited API user for this access

This is a postinstall script embedded in a payloadless.pkg.
The package is a stub installer that can be tied to any or all macOS patch definitions in Jamf Pro, regardless of version, but will always install "Latest"
The Patch Policy provides the logic to see if this script will be avilable to the client
When the script runs, it will use the API to see which version of macOS is the "Latest", then check 'softwareupdate' for the patch, then install it
Additional message can be provided via Jamf Helper because the script won't provide the user any feedback that the pkg is either downloading, installing, or both
This script can be used across multiple customers as long as the "variables" are all true:

Server URL = Designate a "master" Jamf Pro Server to maintain the patch definition for macOS
Create the API user and provide privileges (Patch Management Titles = READ)
Tip: Create an "Update Inventory" policy for "Startup" trigger for "All Computers". That will calculate our smart groups and patch policy scopes after we run the update
