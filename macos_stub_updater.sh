#!/bin/bash


#######################################
# 2020 ARMOUR PEAK LLC - Brian Hanson 
#
# Use the Jamf API to leverage the patch definitions for latest version of macOS
# Once we have the version, call softwareupdate to install this OS patch only
# We may or may not have the latest patch from Apple already cached, but that's ok
# Make sure to create a limited API user for this access
#
# This is a postinstall script embedded in a payloadless.pkg so that we can connect a .pkg to a patch definition. Future plans may include just a curl in the .pkg to download this script.
# The package is a stub installer that can be tied to any or all macOS patch definitions in Jamf Pro, regardless of version, but will always install "Latest"
# The Patch Policy provides the logic to see if this script will be avilable to the client
# When the script runs, it will use the API to see which version of macOS is the "Latest", then check 'softwareupdate' for the patch, then install it
# Additional message can be provided via Jamf Helper because the script won't provide the user any feedback that the pkg is either downloading, installing, or both
# This script can be used across multiple customers as long as the "variables" are all true:
# 
# Server URL = Designate a "master" Jamf Pro Server to maintain the patch definition for macOS
# Create the API user and provide privileges (Patch Management Titles = READ)
# Tip: Create an "Update Inventory" policy for "Startup" trigger for "All Computers". That will calculate our smart groups and patch policy scopes after we run the update
#
#######################################

#######################################
# Create an XSLT file at /tmp/patch_id.xslt to parse the patch ID based on the patch name
#######################################
cat << EOF > /tmp/patch_id.xslt
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
<xsl:template match="/"> 
	<xsl:for-each select="software_title"> 
		<xsl:value-of select="id"/>
		<xsl:text>&#xa;</xsl:text> 
	</xsl:for-each> 
</xsl:template> 
</xsl:stylesheet>
EOF
#######################################
#######################################
# Create an XSLT file at /tmp/patch_version.xslt to parse the version number of the patch ID
#######################################
cat << EOF > /tmp/patch_version.xslt
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>
<xsl:template match="/"> 
	<xsl:for-each select="patch_report/versions/version"> 
		<xsl:value-of select="software_version"/>
		<xsl:text>&#xa;</xsl:text> 
	</xsl:for-each> 
</xsl:template> 
</xsl:stylesheet>
EOF
#######################################

#######################################
# We only need to ask one Jamf Pro server about this patch, just make sure it exists. Otherwise point to customer server and create an API user there
SERVER=https://armourpeaknfr.jamfcloud.com
CREDS="cGF0Y2hfYXBpX3JlYWQ6SWoyRCpjKGFyaypAbidoSEFmW2thM3N3"

# Setup the Jamf Helper Prompt
show_prompt() {
# Make sure to end the command with an ampersand (&) or else will hang everyhting
"/Library/Application Support/JAMF/bin/jamfHelper.app/Contents/MacOS/jamfHelper" -windowType hud -windowPosition ur -title "$1" -heading "$2" -alignHeading natural -description "$3" -alignDescription natural -lockHUD -icon "/System/Library/CoreServices/Software Update.app/Contents/Resources/SoftwareUpdate.icns" &
}
# Let's get the id of the macOS patch
patch_id=$(curl -X GET --header "Authorization: Basic $CREDS" $SERVER/JSSResource/patches/name/macOS | xsltproc /tmp/patch_id.xslt -)

# Let's get the latest version of the patch id
patch_version=$(curl -su "$API_USER":"$API_PASS" $SERVER/JSSResource/patchreports/patchsoftwaretitleid/$patch_id/version/Latest | xsltproc /tmp/patch_version.xslt -)

# Format the version for the current known naming format for OS updates. Note the space after the - at the end of the label. Apple may *fix* this at some point
target_version_label=$(echo "$patch_version Update- ")

# Let's make sure we actually have this update available to using softwareupdate. This means the OS knows we are not on this version and we don't need more logic than that
# We may not have it because even though we qualify for updates (according to patch manegemnt) we may need an OS upgrade instead of a dot release. More work is needed here to merge the two tools, but this works for patches for now
update_available=$(softwareupdate --list | grep "$target_version_label" )

if [[ $update_available ]]; then
	
	# Add Jamf Help message here
	show_prompt "Software Update" "Updates Running..." "Your Mac will restart momentarily, please close any unsaved files."
	echo "Your Mac is about to restart, please save and quit all your apps..." 
	softwareupdate --install "$target_version_label" --restart

else

	echo "No update" 

fi


